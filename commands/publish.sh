#!/bin/bash
set -e
set -x
set -o pipefail
set -o nounset
set -o errexit
set -o errtrace
set -o functrace

pelican content -s publishconf.py -t theme/
