Title: Thanks
Date: 2024-02-19 12:00
Tags: meta
Slug: acknowledgements
Authors: Josh Albrecht
Summary: Some random shoutouts

Thanks to everyone at <a href="https://imbue.com">Imbue</a> for all of the ideas and support, especially <a href="https://kanjun.me/">Kanjun</a>.
All of the good ideas are probably theirs, and all of the bad ones are probably mine.

This site is built with <a href="http://www.python.org">Python</a>, <a href="http://getpelican.com">Pelican</a>, and based on the <a href="https://github.com/hdra/Pelican-Cid">Cid</a> theme.
