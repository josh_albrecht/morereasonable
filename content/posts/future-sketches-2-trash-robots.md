Title: Future Sketches 2: Trash Robots
Date: 2024-02-20 12:00
Tags: future-sketches, robots, nature
Slug: future-sketches-2-trash-robots
Authors: Josh Albrecht
Summary: What if we didn't live in a world where trash was dumped on the side of the road?
ArticleType: scene
ShareURL: https://twitter.com/



We don't have to live in a world where trash is dumped on the side of the road…
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_the_view_while_driving_through_a_city_street._pil_0e79d161-b2e0-40f9-90fb-eca688ee391f.png)



Imagine a world with highly capable robots...
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_friendly_robot_balancing_on_one_leg_on_a_stone__5f08a734-8e6d-4acd-b9f6-6862c2f411dc.png)




Where the cost of physical labor has dropped to nearly zero...
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_fields_being_harvested_by_robots._cinematic_anime_58a6fade-2379-4b4f-bcba-afdee9e84494.png)



If we develop affordable, general purpose robotics...
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_grad_student_having_fun_training_a_cute_little__fa6804cb-fa1d-4e6c-bcda-45a21e264bf2.png)



Why shouldn't we create robots that are designed to clean up the side of the road?
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_robots_picking_up_trash_along_the_side_of_the_roa_ac4c2428-9fbc-4615-8854-080ebe9beb97.png)



And why stop there?  Why not clean up our landfills?
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_friendly_clean_futuristic_robots_working_together_adf8357b-4c26-4dd4-aedb-0ce7b2b6b223.png)




Or better yet, sort through them for valuable materials that we failed to recycle?
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_futuristic_factory_with_friendly_robots_sorting_75ae0a4c-99e7-46f3-819b-837c3ca021bd.png)




With sufficiently cheap robotics, why not replant the forests we destroyed?
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_friendly-looking_cute_little_robots_working_toget_4366f5c8-6edd-4d85-a5ab-5cf704c682e9.png)




Why not help raise baby endangered animals?
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_cute_robot_playing_with_a_cute_little_baby_tige_77154a17-704f-4c73-92eb-8978c90a4ec9.png)




And give them a beautiful environment in which to thrive?
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_beautiful_clear_stream_running_through_a_sunny__3dc269d1-5637-43c9-9d36-c018e5357e3c.png)






We can clean up our world.
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_people_and_robots_working_together_picking_up_tra_28b103f9-a7ca-41ec-820a-cf326dc60770.png)




We can create a world where robots, and technologies more generally, are used for conservationism, for protecting the natural world, for reversing the damage we've done, and for helping humanity live in harmony with the natural world.
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_bird_taking_flight_out_of_a_futuristic_bird_hou_c8a94b62-cd95-442e-8376-81c1c97b123f.png)



We can make it better than it ever was before.
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_world_where_humanity_has_improved_on_the_natura_771a6daa-5856-4ebe-a856-daaba4214086.png)



That's a world we can choose.






<div id="author-notes" class="author-notes" style="padding-bottom: 4em; padding-top: 12em">
    <h3>Author's notes</h3>
    <div>If you're excited about that world, see these labs and companies who are doing great work on robotics</div>
</div>




