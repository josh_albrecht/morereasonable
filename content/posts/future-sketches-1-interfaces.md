Title: Future Sketches 1: Interfaces
Date: 2024-02-19 13:00
Tags: future-sketches, interfaces
Slug: future-sketches-1-interfaces
Authors: Josh Albrecht
Summary: What might the future of interfaces look like?
ArticleType: scene
ShareURL: https://twitter.com/



Imagine the not-too-distant future…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_scene_setting_shot_of_san_francisco_in_the_near_d53e3c5a-89e8-4b0e-99fd-ea5114c258dd.png)




Where people have flooded back to San Francisco to enjoy a renaissance sparked by AI…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_downtown_san_francisco_busy_full_of_people_slight_fdfe73f7-10ae-479d-b044-09e4682f8772.png)




A world where people work less and enjoy life more…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_office_scene_people_playing_around_and_having_fun_f76281ed-b973-4478-9f7d-3dd036110673.png)






Where the menial tasks are taken care of by our computers…
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_minimalistic_sketch_black_lines_on_a_white_backgr_f775c5b7-fe51-4075-b4c2-4bc4e6054a61.png)







And we are free to pursue creative work…


![](https://paper-attachments.dropboxusercontent.com/s_C72274848AB471B1C4F44C51850C3C5C1F5E15D5D149C6EFD83024FF95125C3E_1704259390239_must_grey-modified.png)






Or help our fellow humans in need.
![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_people_helping_build_new_houses_after_desctructio_66fe45dc-af4e-4f12-b29b-babad6817187.png)








In this world, computer software is written not by elite hackers in the dead of night…



![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_minimalistic_sketch_white_lines_on_a_black_backgr_7bb9f4a2-f3ce-4c1f-9ae7-9332fe4e6d1e.png)






But by everyone, by having our computers fill in the boring details…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_robot_filling_out_stacks_of_tax_forms_storyboar_d5fc52ee-5d0f-4899-abfb-5d5413f1288f.png)









While we can focus on big picture and the high-level ideas.


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_hand_drawing_of_two_people_crea_1cc64515-b45c-4354-b7fe-788882a69148.png)








In this world, computers become something different, something new, something magical…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_hand_drawing_of_a_computer_that_e93d621b-7ae4-4a8b-aafd-797e2ca47ccd.png)







Something that we’ve seen before only in science fiction movies…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_hand_sketch_of_a_woman_using_a__9e1159f2-7330-4f9a-9011-7c4592ee7759.png)







But in this world, these technologies are not restricted to a few superheros…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_hand_sketch_with_a_black_background_white_lines.__5d32473c-5a39-4520-a39e-911021a86dc0.png)







They are free for all to use


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_hand_drawing_of_children_playin_6826b3a1-6976-4032-ba95-0794389bbd8c.png)








They become a part of our environment…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_hand_sketch_of_a_kitchen_with_p_360e568d-ba8b-4895-baee-258b3cbb94a7.png)








Seamlessly taking care of the details of everyday life…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_hand_sketch_of_a_woman_walking__213345dc-2bf1-497e-9642-00b8a1ddb825.png)







So that we can focus on the people that matter to us.


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_sketch_of_a_husband_and_wife_ha_6966fb93-0b4b-487c-b445-91303a5657ff.png)







This world comes with its own challenges…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_sketch_of_a_very_long_line_of_p_f543056f-4b1a-4e3c-8998-a28ca3fa22e5.png)







But in a world where most human minds are freed from the drudgery of mental labor…


![](https://d3n6320cxm4s8m.cloudfront.net/images/josh_albrecht_a_black_and_white_sketch_of_thousands_of_computer_6847a3da-9223-47e2-9a0d-373877b051b3.png)





Will we not solve them?

















<div id="author-notes" class="author-notes" style="padding-bottom: 4em; padding-top: 12em">
    <h3>Author's notes</h3>
    <div>h/t to <a href="https://www.badspacecomics.com/post/empires">Empires by Scott Base</a> for the artistic inspiration, and thx to <a href="https://twitter.com/kanjun?lang=en">@kanjun</a> for many of the ideas</div>
</div>


