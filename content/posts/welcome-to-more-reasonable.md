Title: Welcome to More Reasonable
Date: 2024-02-19 12:00
Tags: meta
Slug: welcome-to-more-reasonable
Authors: Josh Albrecht
Summary: A short overview of the purpose of this site

Hi!

This site is intended as a collection of thoughts about how we might, together, build the futures that we want.

It is often said that pictures are worth a thousand words.
Given how much there is to say about the serious issues facing us as a civilization today, and about the amazing opportunities in front of us, I try to make my posts as visual as possible.

However, I am a terrible visual artist, so my images are generally created using [Midjourney](https://www.midjourney.com/).
Where possible, I try to cite the visual styles of the artists that I invoke in my prompts, but I'm still very much a novice in the world of art.
Please feel free to educate me about my mistakes!

While most of the content here is my own original work, I also hope to someday inspire others to contribute their own visions of the future.
As such, my content here is intended not to advocate for any particular world view or appeal to some particular political party, but to spark conversation--to ask questions about what all of us do, and don't, want from our world.

While I intend to keep this site as apolitical as possible, I do value clear thinking and civil discourse.
I was raised in an era before the internet isolated and polarized us, where op-eds were written by concerned citizens and published by newspapers that still did investigative journalism.
I believe that it is possible to have intelligent conversations about complex issues.
I love to learn ways in which I am wrong, so please feel free to reach out if you see something that is not right!
And if you do link to this site, please do so in a way that elevates our collective conversations.

Finally, my not-so-secret agenda with this site is to move the world towards a less cynical view of the present and the future.
I believe that most (but not all) people, when they are psychologically healthy, tend towards volunteering to help their fellow neighbors, towards benevolence, and towards selflessness, and towards the human virtues and values that inspire many of us.
I am deeply moved by scientific inquiry, curiousity, and the human spirit.
And I am inspired by the millions of people who wake up every day and try to, in their own way, make the world just a little bit better.

This site is dedicated to all of those people.
